package xyz.mynt.gctraining2.facade.dto;

import xyz.mynt.gctraining2.facade.dto.AccountDTO;

public class TransactionDTO {

    private AccountDTO account;

    private String mobileNo;

    private String message;

    private String transactionStatus;

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Getter method for property <tt>transactionStatus</tt>.
     *
     * @return property value of transactionStatus
     */
    public String getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Setter method for property <tt>transactionStatus</tt>.
     *
     * @param transactionStatus value to be assigned to property transactionStatus
     */
    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}