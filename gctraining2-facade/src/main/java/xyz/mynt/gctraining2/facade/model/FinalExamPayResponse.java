package xyz.mynt.gctraining2.facade.model;

public class FinalExamPayResponse {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
