package xyz.mynt.gctraining2.facade.model;

import java.util.Map;

public class FinalExamPayRequest {
    private String productCode;
    private String productEventCode;
    private String requestTransId;
    private String accountId;
    private String mobileNo;
    private String amount;
    private Map<String, String> extendInfo;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductEventCode() {
        return productEventCode;
    }

    public void setProductEventCode(String productEventCode) {
        this.productEventCode = productEventCode;
    }

    public String getRequestTransId() {
        return requestTransId;
    }

    public void setRequestTransId(String requestTransId) {
        this.requestTransId = requestTransId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Map<String, String> getExtendInfo() {
        return extendInfo;
    }

    public void setExtendInfo(Map<String, String> extendInfo) {
        this.extendInfo = extendInfo;
    }
}
