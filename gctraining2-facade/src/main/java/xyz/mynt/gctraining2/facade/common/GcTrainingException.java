package xyz.mynt.gctraining2.facade.common;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


public class GcTrainingException extends RuntimeException implements GcTrainingResponse {

    private static final long serialVersionUID = -6963544947844785521L;

    /**
     * @see CommonResultStatus
     */
    private String            resultStatus;

    /**
     * result code
     */
    private String            resultCode;

    /**
     * constructor
     *
     * @param GcTrainingResponse result code
     */
    public GcTrainingException(GcTrainingResponse GcTrainingResponse) {

        super(GcTrainingResponse.getResultMsg());
        this.resultStatus = GcTrainingResponse.getResultStatus();
        this.resultCode = GcTrainingResponse.getResultCode();

    }

    /**
     * constructor
     *
     * @param GcTrainingResponse result code
     * @param message message to override result code
     */
    public GcTrainingException(GcTrainingResponse GcTrainingResponse, String message) {

        super(message);
        this.resultStatus = GcTrainingResponse.getResultStatus();
        this.resultCode = GcTrainingResponse.getResultCode();

    }

    /**
     * constructor
     *
     * @param GcTrainingResponse result code
     * @param e exception
     */
    public GcTrainingException(GcTrainingResponse GcTrainingResponse, Throwable e) {

        super(e);
        this.resultStatus = GcTrainingResponse.getResultStatus();
        this.resultCode = GcTrainingResponse.getResultCode();

    }

    /**
     * constructor
     *
     * @param GcTrainingResponse result code
     * @param message message to override result code
     * @param e exception
     */
    public GcTrainingException(GcTrainingResponse GcTrainingResponse, String message, Throwable e) {

        super(message, e);
        this.resultStatus = GcTrainingResponse.getResultStatus();
        this.resultCode = GcTrainingResponse.getResultCode();

    }

    @Override
    public boolean getSuccess() {
        return false;
    }

    @Override
    public String getResultStatus() {
        return resultStatus;
    }

    @Override
    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String getResultMsg() {
        return this.getMessage();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
