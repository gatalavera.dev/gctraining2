package xyz.mynt.gctraining2.facade.common;


public interface GcTrainingResponse {

    /**
     * is success
     * @return success
     */
    boolean getSuccess();

    /**
     * get result status
     *
     * @return result status
     */
    String getResultStatus();

    /**
     * get result code
     *
     * @return result code
     */
    String getResultCode();

    /**
     * get result msg
     *
     * @return result msg
     */
    String getResultMsg();

}
