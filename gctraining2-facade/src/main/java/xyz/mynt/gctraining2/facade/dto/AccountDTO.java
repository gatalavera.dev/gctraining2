package xyz.mynt.gctraining2.facade.dto;

import java.util.Date;

public class AccountDTO {

    private String id;

    private Double amount;

    private Date gmtCreate;

    private String sourceAppId;


    /**
     * Getter method for property <tt>amount</tt>.
     *
     * @return property value of amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Setter method for property <tt>amount</tt>.
     *
     * @param amount value to be assigned to property amount
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * Getter method for property <tt>gmtCreate</tt>.
     *
     * @return property value of gmtCreate
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * Setter method for property <tt>gmtCreate</tt>.
     *
     * @param gmtCreate value to be assigned to property gmtCreate
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * Getter method for property <tt>id</tt>.
     *
     * @return property value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter method for property <tt>id</tt>.
     *
     * @param id value to be assigned to property id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter method for property <tt>getSourceAppId</tt>.
     *
     * @return property value of getSourceAppId
     */
    public String getSourceAppId() {
        return sourceAppId;
    }

    /**
     * Setter method for property <tt>sourceAppId</tt>.
     *
     * @param sourceAppId value to be assigned to property sourceAppId
     */
    public void setSourceAppId(String sourceAppId) {
        this.sourceAppId = sourceAppId;
    }
}