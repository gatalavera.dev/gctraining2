package xyz.mynt.gctraining2.facade.model;

public class AuthenticateRequest {

    private String loginName;
    private String loginPassword;

    /**
     * Getter method for property <tt>loginName</tt>.
     *
     * @return property value of loginName
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * Setter method for property <tt>loginName</tt>.
     *
     * @param loginName value to be assigned to property loginName
     */
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    /**
     * Getter method for property <tt>loginPassword</tt>.
     *
     * @return property value of loginPassword
     */
    public String getLoginPassword() {
        return loginPassword;
    }

    /**
     * Setter method for property <tt>loginPassword</tt>.
     *
     * @param loginPassword value to be assigned to property loginPassword
     */
    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }
}
