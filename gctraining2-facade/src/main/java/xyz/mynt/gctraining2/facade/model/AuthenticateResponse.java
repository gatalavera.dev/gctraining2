package xyz.mynt.gctraining2.facade.model;

public class AuthenticateResponse {

    private String transId;

    /**
     * Getter method for property <tt>transId</tt>.
     *
     * @return property value of transId
     */
    public String getTransId() {
        return transId;
    }

    /**
     * Setter method for property <tt>transId</tt>.
     *
     * @param transId value to be assigned to property transId
     */
    public void setTransId(String transId) {
        this.transId = transId;
    }
}
