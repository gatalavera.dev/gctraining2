/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package xyz.mynt.gctraining2.facade.common;


public enum CommonResultStatus {

    /**
     * success
     */
    S("success"),

    U("unknown"),

    F("failed");


    /**
     * desc
     */
    private String desc;

    /**
     * construct
     *
     * @param desc desc
     */
    CommonResultStatus(String desc) {

        this.desc = desc;

    }

    /**
     * is success
     *
     * @return success
     */
    public static boolean isSuccess(String resultStatus) {

        return S.getCode().equals(resultStatus);

    }

    /**
     * is failed
     *
     * @return failed
     */
    public static boolean isFailed(String resultStatus) {

        return U.getCode().equals(resultStatus);

    }

    /**
     * is unknown
     *
     * @return unknown
     */
    public static boolean isUnknown(String resultStatus) {

        return !isSuccess(resultStatus) && !isFailed(resultStatus);

    }

    public String getCode() {

        return name();

    }

    /**
     * Getter method for property <tt>desc</tt>.
     *
     * @return property value of desc
     */
    public String getDesc() {
        return desc;
    }

}
