package xyz.mynt.gctraining2.facade.model;

public class Data {
    private String transId;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }
}
