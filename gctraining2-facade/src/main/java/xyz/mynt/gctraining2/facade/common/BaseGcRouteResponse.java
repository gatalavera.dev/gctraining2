/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package xyz.mynt.gctraining2.facade.common;

import com.alipay.ap.router.client.structures.Responseable;
import com.alipay.ap.router.client.structures.ResultInfo;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class BaseGcRouteResponse<T> implements Responseable {

    /**
     * result info
     */
    private ResultInfo resultInfo;

    /**
     * data
     */
    private T          data;

    /**
     * construct
     *
     * @param response response
     */
    public BaseGcRouteResponse(GcTrainingResponse response) {
        this.resultInfo = new ResultInfo();
        this.resultInfo.setResultStatus(response.getResultStatus());
        this.resultInfo.setResultCodeId(response.getResultCode());
        this.resultInfo.setResultCode(response.getResultCode());
        this.resultInfo.setResultMsg(response.getResultMsg());
    }

    /**
     * construct
     *
     * @param data data
     */
    public BaseGcRouteResponse(GcTrainingResponse response, T data) {
        this(response);
        this.data = data;
    }

    @Override
    public ResultInfo getResultInfo() {
        return resultInfo;
    }

    /**
     * Getter method for property <tt>data</tt>.
     *
     * @return property value of data
     */
    public T getData() {
        return data;
    }

    /**
     * Setter method for property <tt>data</tt>.
     *
     * @param data value to be assigned to property data
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
