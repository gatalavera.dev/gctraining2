package xyz.mynt.gctraining2.facade.pay;

import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.model.FinalExamPayRequest;
import xyz.mynt.gctraining2.facade.model.FinalExamPayResponse;

public interface FinalExamPayService {
    GcTrainingResult<FinalExamPayResponse> validateFinalPay(FinalExamPayRequest finalExamPayRequest);
}
