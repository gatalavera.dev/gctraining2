/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package xyz.mynt.gctraining2.facade.common;


public enum GcTrainingResultCode implements GcTrainingResponse {

    /**
     * success
     */
    SUCCESS(CommonResultStatus.S, "success"),

    /**
     * Invalid Username
     */
    INVALID_USERNAME(CommonResultStatus.F, "Invalid Username"),

    /**
     * Invalid Password
     */
    INVALID_PASSWORD(CommonResultStatus.F, "Invalid Password"),

    /**
     * System Error
     */
    SYSTEM_ERROR(CommonResultStatus.U, "System Error"),

    FAILED(CommonResultStatus.F, "Failed"),

    INVALID_PARAM(CommonResultStatus.F, "Invalid Parameter"),

    USER_NOT_FOUND(CommonResultStatus.F, "User not found"),

    DB_ERROR(CommonResultStatus.F, "Database Error");


    /**
     * result status
     */
    private String resultStatus;

    /**
     * error msg
     */
    private String errorMsg;

    /**
     * Construct
     *
     * @param errorMsg error msg
     */
    GcTrainingResultCode(String errorMsg) {

        this(CommonResultStatus.F, errorMsg);
    }

    /**
     * Construct
     *
     * @param errorMsg error msg
     */
    GcTrainingResultCode(CommonResultStatus resultStatus, String errorMsg) {

        this.resultStatus = resultStatus.getCode();
        this.errorMsg = errorMsg;
    }

    @Override
    public boolean getSuccess() {
        return this == SUCCESS;
    }

    @Override
    public String getResultStatus() {
        return resultStatus;
    }

    @Override
    public String getResultCode() {
        return name();
    }

    @Override
    public String getResultMsg() {
        return errorMsg;
    }
}
