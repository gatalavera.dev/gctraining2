package xyz.mynt.gctraining2.facade.common;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


public class GcTrainingResult<T> implements GcTrainingResponse {

    /**
     * @see CommonResultStatus
     */
    private String resultStatus;

    /**
     * result code
     */
    private String resultCode;

    /**
     * result msg
     */
    private String resultMsg;

    /**
     * data
     */
    private T      data;

    /**
     * constructor
     *
     * @param response base
     */
    public GcTrainingResult(GcTrainingResponse response) {

        this.resultStatus = response.getResultStatus();
        this.resultCode = response.getResultCode();
        this.resultMsg = response.getResultMsg();

    }

    /**
     * constructor
     * @param data data
     */
    public GcTrainingResult(GcTrainingResponse response, T data) {
        this(response);
        this.data = data;
    }

    /**
     * constructor
     * @param resultMsg
     * @param data data
     */
    public GcTrainingResult(GcTrainingResponse response, String resultMsg, T data) {
        this(response);
        this.resultMsg = resultMsg;
        this.data = data;
    }

    @Override
    public boolean getSuccess() {
        return CommonResultStatus.isSuccess(resultStatus);
    }

    /**
     * Getter method for property <tt>resultStatus</tt>.
     *
     * @return property value of resultStatus
     */
    @Override
    public String getResultStatus() {
        return resultStatus;
    }

    /**
     * Getter method for property <tt>resultCode</tt>.
     *
     * @return property value of resultCode
     */
    @Override
    public String getResultCode() {
        return resultCode;
    }

    /**
     * Getter method for property <tt>resultMsg</tt>.
     *
     * @return property value of resultMsg
     */
    @Override
    public String getResultMsg() {
        return resultMsg;
    }

    /**
     * Setter method for property <tt>resultMsg</tt>.
     *
     * @param resultMsg value to be assigned to property resultMsg
     */
    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    /**
     * Getter method for property <tt>data</tt>.
     *
     * @return property value of data
     */
    public T getData() {
        return data;
    }

    /**
     * Setter method for property <tt>data</tt>.
     *
     * @param data value to be assigned to property data
     */
    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
