package xyz.mynt.gctraining2.facade.authentication;

import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.model.AuthenticateRequest;
import xyz.mynt.gctraining2.facade.model.AuthenticateResponse;


public interface AuthenticationService {

    GcTrainingResult<AuthenticateResponse> authenticate(AuthenticateRequest authenticateRequest);
}