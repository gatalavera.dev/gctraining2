package xyz.mynt.gctraining2.ddcs;

import com.alipay.drm.client.DRMClient;
import com.alipay.drm.client.api.annotation.AfterUpdate;
import com.alipay.drm.client.api.annotation.BeforeUpdate;
import com.alipay.drm.client.api.annotation.DAttribute;
import com.alipay.drm.client.api.annotation.DObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@DObject(region = "AntCloud", appName = "gctraining2final", id = "xyz.mynt.gctraining2.ddcs.GcTraining2FinalExamPayResource")
@Service
public class GcTraining2FinalExamPayResource implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(GcTraining2userNameAndPassResource.class);

    @DAttribute
    private String mobileNumber;

    private static List<String> mobileNumberList;

    @BeforeUpdate
    public void before(String key, Object value) {
        LOGGER.info("Before-" + key + "=" + value);
    }

    @AfterUpdate
    public void after(String key, Object value) {
        LOGGER.info("After-" + key + "=" + value);
    }

    public static Logger getLOGGER() {
        return LOGGER;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
        mobileNumberList = convertMobileNumber(mobileNumber);
    }

    public List<String> getMobileNumberList() {
        return mobileNumberList;
    }

    public void setMobileNumberList(List<String> mobileNumberList) {
        this.mobileNumberList = mobileNumberList;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        DRMClient.getInstance().register(this);
    }

    public static boolean validate(String mobileNumber) {
        return mobileNumberList.contains(mobileNumber);
    }

    private List<String> convertMobileNumber(String mobileNumber) {
        String[] result = mobileNumber.replaceAll("[\r|\n|\t]", "").split("|");
        List<String> mobileNumbers = new ArrayList<>();
        for (String mobile : result) {
            mobileNumbers.add(mobile);
        }
        return mobileNumbers;
    }
}
