package xyz.mynt.gctraining2.ddcs;

import com.alibaba.common.lang.ArrayUtil;
import com.alibaba.common.lang.StringUtil;
import com.alipay.drm.client.DRMClient;
import com.alipay.drm.client.api.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;


@DObject(region = "AntCloud", appName = "gctraining2", id = "xyz.mynt.gctraining2.ddcs.GcTraining2userNameAndPassResource")
@Service
public class GcTraining2userNameAndPassResource implements InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(GcTraining2userNameAndPassResource.class);

    @DAttribute
    private String gcTraining2userNameAndPass;

    private static Map<String, String> gcTraining2userNameAndPassMap;

    private static final String GROUP_SPLIT_CHARACTER = ";";

    private static final String ITEM_SPLIT_CHARACTER = ",";

    @BeforeUpdate
    public void before(String key, Object value) {
        LOGGER.info("Before-" + key + "=" + value);
    }

    @AfterUpdate
    public void after(String key, Object value) {
        LOGGER.info("After-" + key + "=" + value);
    }

    private Map<String, String> convertgcTraining2userNameAndPassToMap(String gcTraining2userNameAndPass) {

        Map<String, String> tempUsernameAndPass = new HashMap<>();

        if (StringUtil.isBlank(gcTraining2userNameAndPass)) {
            return tempUsernameAndPass;
        }

        gcTraining2userNameAndPass = gcTraining2userNameAndPass.replaceAll("[\r|\n|\t]", "");

        String[] gcTraining2UsernameandPassArray = gcTraining2userNameAndPass.split(GROUP_SPLIT_CHARACTER);
        if (ArrayUtil.isEmpty(gcTraining2UsernameandPassArray)) {
            return tempUsernameAndPass;
        }

        for (String gcTraining2UsernameandPass : gcTraining2UsernameandPassArray) {
            String[] usernameAndPassArray = gcTraining2UsernameandPass.trim().split(ITEM_SPLIT_CHARACTER);
            if (usernameAndPassArray.length >= 2) {
                String username = usernameAndPassArray[0];
                String password = usernameAndPassArray[1];
                tempUsernameAndPass.put(username, password);
            }
        }
        return tempUsernameAndPass;
    }

    public static Map<String, String> getGcTraining2userNameAndPassMap() {
        return gcTraining2userNameAndPassMap;
    }

    public static void setGcTraining2userNameAndPassMap(Map<String, String> gcTraining2userNameAndPassMap) {
        GcTraining2userNameAndPassResource.gcTraining2userNameAndPassMap = gcTraining2userNameAndPassMap;
    }

    public void setGcTraining2userNameAndPass(String gcTraining2userNameAndPass) {
        this.gcTraining2userNameAndPass = gcTraining2userNameAndPass;
        gcTraining2userNameAndPassMap = convertgcTraining2userNameAndPassToMap(gcTraining2userNameAndPass);
    }

    public String getGcTraining2userNameAndPass() {
        return gcTraining2userNameAndPass;
    }

    public void setGcTraining16userNameAndPass(String gcTraining2userNameAndPass) {
        this.gcTraining2userNameAndPass = gcTraining2userNameAndPass;
        gcTraining2userNameAndPassMap = convertgcTraining2userNameAndPassToMap(gcTraining2userNameAndPass);
    }

    public static boolean validateUsername(String username) {

        Map<String, String> userNameAndPassMap = gcTraining2userNameAndPassMap;
        if (CollectionUtils.isEmpty(userNameAndPassMap)) {
            return false;
        }

        return userNameAndPassMap.containsKey(username);
    }


    public static boolean validatePassword(String username, String password) {

        Map<String, String> userNameAndPassMap = gcTraining2userNameAndPassMap;
        if (CollectionUtils.isEmpty(userNameAndPassMap)) {
            return false;
        }

        return userNameAndPassMap == null ? false : userNameAndPassMap.get(username).equals(password);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        DRMClient.getInstance().register(this);
    }

}
