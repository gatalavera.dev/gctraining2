package xyz.mynt.gctraining2.service.api.impl;

import com.alibaba.common.logging.Logger;
import com.alibaba.common.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.mynt.gctraining2.facade.common.BaseGcRouteResponse;
import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.model.FinalExamPayRequest;
import xyz.mynt.gctraining2.facade.model.FinalExamPayResponse;
import xyz.mynt.gctraining2.facade.pay.FinalExamPayService;
import xyz.mynt.gctraining2.service.api.FinalExamServiceGcRouter;
import xyz.mynt.gctraining2.service.api.template.ControllerLayerTemplate;

@Service
public class FinalExamServiceGcRouterImpl implements FinalExamServiceGcRouter {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(FinalExamServiceGcRouterImpl.class);

    @Autowired
    FinalExamPayService finalExamPayService;

    @Override
    public BaseGcRouteResponse<FinalExamPayResponse> authenticate(FinalExamPayRequest request) {
        GcTrainingResult<FinalExamPayResponse> result = ControllerLayerTemplate.execute(LOGGER,
                "FinalExamServiceGcRouter.authenticate",
                () -> finalExamPayService.validateFinalPay(request));
        return new BaseGcRouteResponse<>(result, result.getData());
    }
}
