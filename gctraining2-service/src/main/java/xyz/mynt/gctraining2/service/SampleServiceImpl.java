package xyz.mynt.gctraining2.service;


import xyz.mynt.gctraining2.facade.SampleService;

public class SampleServiceImpl implements SampleService {

    @Override
    public String message() {
        return "Hello, Service slitecore";
    }
}
