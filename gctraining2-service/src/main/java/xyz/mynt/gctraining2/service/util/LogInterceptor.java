/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package xyz.mynt.gctraining2.service.util;

import com.alibaba.common.logging.Logger;
import com.alibaba.common.logging.LoggerFactory;
import com.alipay.fc.common.util.log.BaseInterceptor;
import com.alipay.fc.common.util.log.LogUtil;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;
import java.util.Arrays;

;

/**
 * log interceptor
 *
 * @author huakang.hk
 * @version $Id: LogInterceptor.java, v 0.1 2019-04-28 17:51 huakang.hk Exp $
 */
public class LogInterceptor extends BaseInterceptor {

    /**
     * logger
     */
    private Logger logger;

    @Override
    public Object businessInvoke(MethodInvocation invocation) throws Throwable {

        Method method = invocation.getMethod();
        String simpleMethodName = method.getDeclaringClass().getSimpleName() + "."
                + method.getName();

        Object[] args = null;
        Object result = null;

        try {

            args = invocation.getArguments();

            LogUtil.info(this.logger, simpleMethodName, " start, invoke params : ",
                    Arrays.toString(args));

            result = invocation.proceed();

            LogUtil.info(this.logger, simpleMethodName, " end, invoke params : ",
                    Arrays.toString(args), " invoke result : ", result);

        } catch (Throwable e) {

            LogUtil.warn(this.logger, e, simpleMethodName, " error, invoke params : ",
                    Arrays.toString(args));

            throw e;

        }

        return result;
    }

    /**
     * set logger name
     *
     * @param loggerName
     */
    public void setLoggerName(String loggerName) {
        this.logger = LoggerFactory.getLogger(loggerName);
    }

}