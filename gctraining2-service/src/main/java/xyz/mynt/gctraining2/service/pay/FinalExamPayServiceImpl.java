package xyz.mynt.gctraining2.service.pay;

import com.alipay.fc.custcenter.common.domain.enums.LoginIdTypeEnum;
import io.netty.util.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import xyz.mynt.gctraining16.facade.dto.AccountDTO;
import xyz.mynt.gctraining2.dal.dataobject.TransactionDO;
import xyz.mynt.gctraining2.dal.mybatis.TransactionMybatisDAO;
import xyz.mynt.gctraining2.ddcs.GcTraining2FinalExamPayResource;
import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.common.GcTrainingResultCode;
import xyz.mynt.gctraining2.facade.model.Data;
import xyz.mynt.gctraining2.facade.model.FinalExamPayRequest;
import xyz.mynt.gctraining2.facade.model.FinalExamPayResponse;
import xyz.mynt.gctraining2.facade.pay.FinalExamPayService;
import xyz.mynt.gctraining2.mq.AsyncPayService;
import xyz.mynt.user.facade.UserQueryFacade;
import xyz.mynt.user.facade.request.QueryUserInfoRequest;
import xyz.mynt.user.facade.response.QueryUserInfoResponse;

import java.util.Date;
import java.util.UUID;

public class FinalExamPayServiceImpl implements FinalExamPayService {

    @Autowired
    UserQueryFacade userQueryFacade;

    @Autowired
    TransactionMybatisDAO transactionDAO;

    @Autowired
    AsyncPayService asyncPayService;

    @Override
    public GcTrainingResult<FinalExamPayResponse> validateFinalPay(FinalExamPayRequest finalExamPayRequest) {
        FinalExamPayResponse finalExamPayResponse = new FinalExamPayResponse();

        if (StringUtil.isNullOrEmpty(finalExamPayRequest.getAccountId())) {
            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_PARAM, finalExamPayResponse);
        }

        if (StringUtil.isNullOrEmpty(finalExamPayRequest.getAmount())) {
            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_PARAM, finalExamPayResponse);
        }

        if (StringUtil.isNullOrEmpty(finalExamPayRequest.getMobileNo())) {
            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_PARAM, finalExamPayResponse);
        }

        if (StringUtil.isNullOrEmpty(finalExamPayRequest.getProductCode())) {
            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_PARAM, finalExamPayResponse);
        }

        if (StringUtil.isNullOrEmpty(finalExamPayRequest.getProductEventCode())) {
            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_PARAM, finalExamPayResponse);
        }

        if (StringUtil.isNullOrEmpty(finalExamPayRequest.getRequestTransId())) {
            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_PARAM, finalExamPayResponse);
        }

        QueryUserInfoRequest request = GcTraining2FinalExamPayResource.validate(finalExamPayRequest.getMobileNo()) ?
                composeGCuserRequest(finalExamPayRequest.getAccountId()) :
                composeGCuserRequest(finalExamPayRequest.getMobileNo());

//        if (GcTraining2FinalExamPayResource.validate(finalExamPayRequest.getMobileNo())){
//            request = composeGCuserRequest(finalExamPayRequest.getAccountId());
//        } else {
//            request = composeGCuserRequest(finalExamPayRequest.getMobileNo());
//        }

        // query gcuser user info
        QueryUserInfoResponse response = userQueryFacade.queryUserInfo(request);


        if ( response == null ) {
            return new GcTrainingResult<>(GcTrainingResultCode.USER_NOT_FOUND, finalExamPayResponse);
        }
        Data data = new Data();
        try {
            UUID uuid;
            String randomUUIDString;
            TransactionDO transactionDO = new TransactionDO();
            do {
                uuid = UUID.randomUUID();
                randomUUIDString = uuid.toString();
            } while (transactionDAO.selectTransaction(randomUUIDString) != null);
            transactionDO.setTrans_id(randomUUIDString);
            transactionDO.setAmount(finalExamPayRequest.getAmount());
            transactionDO.setMobileNumber(finalExamPayRequest.getMobileNo());
            transactionDAO.insertTransaction(transactionDO);
            data.setTransId(randomUUIDString);
        } catch (Exception e) {
            return new GcTrainingResult<>(GcTrainingResultCode.DB_ERROR, finalExamPayResponse);
        }
        //compose response for GcTrainingResult -> finalExamPayResponse
        finalExamPayResponse.setData(data);
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(finalExamPayResponse.getData().getTransId());
        accountDTO.setAmount(Double.parseDouble(finalExamPayRequest.getAmount()));
        accountDTO.setGmtCreate(new Date());
        accountDTO.setSourceAppId(UUID.randomUUID().toString());

        // publish message to message publisher
        asyncPayService.publish(accountDTO);

        return new GcTrainingResult<>(GcTrainingResultCode.SUCCESS, finalExamPayResponse);
    }

    //compose request for gcUser
    private QueryUserInfoRequest composeGCuserRequest(String msisdn) {
        QueryUserInfoRequest request = new QueryUserInfoRequest();
        request.setUserCode(msisdn);
        request.setUserType(LoginIdTypeEnum.MOBILE_NO.getCode());

        return request;
    }
}
