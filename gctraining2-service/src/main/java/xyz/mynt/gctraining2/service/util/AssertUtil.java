/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package xyz.mynt.gctraining2.service.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import xyz.mynt.gctraining2.facade.common.GcTrainingException;
import xyz.mynt.gctraining2.facade.common.GcTrainingResultCode;

import java.util.Collection;
import java.util.Map;


public class AssertUtil {

    /**
     * is true
     * @param expValue expValue
     * @param resultCode resultCode
     * @param objects objects
     */
    public static void isTrue(boolean expValue, GcTrainingResultCode resultCode, Object... objects) {
        if (!expValue) {
            String logString = getLogString(objects);
            throw new GcTrainingException(resultCode, logString);
        }
    }

    /**
     * is blank
     * @param str str
     * @param resultCode resultCode
     @param objects objects
     */
    public static void isBlank(String str, GcTrainingResultCode resultCode, Object... objects) {
        isTrue(StringUtils.isBlank(str), resultCode, objects);
    }

    /**
     * not blank
     * @param str str
     * @param resultCode resultCode
     @param objects objects
     */
    public static void notBlank(String str, GcTrainingResultCode resultCode, Object... objects) {
        isTrue(StringUtils.isNotBlank(str), resultCode, objects);
    }

    /**
     * is null
     * @param object object
     * @param resultCode resultCode
     @param objects objects
     */
    public static void isNull(Object object, GcTrainingResultCode resultCode, Object... objects) {
        isTrue(object == null, resultCode, objects);
    }

    /**
     * not null
     * @param object object
     * @param resultCode resultCode
     @param objects objects
     */
    public static void notNull(Object object, GcTrainingResultCode resultCode, Object... objects) {
        isTrue(object != null, resultCode, objects);
    }

    /**
     * is empty
     * @param collection collection
     * @param resultCode resultCode
     @param objects objects
     */
    public static void isEmpty(Collection collection, GcTrainingResultCode resultCode,
                               Object... objects) {
        isTrue(CollectionUtils.isEmpty(collection), resultCode, objects);
    }

    /**
     * not empty
     * @param collection collection
     * @param resultCode resultCode
     @param objects objects
     */
    public static void notEmpty(Collection collection, GcTrainingResultCode resultCode,
                                Object... objects) {
        isTrue(!CollectionUtils.isEmpty(collection), resultCode, objects);
    }

    /**
     * is empty
     * @param map map
     * @param resultCode resultCode
     @param objects objects
     */
    public static void isEmpty(Map map, GcTrainingResultCode resultCode, Object... objects) {
        isTrue(CollectionUtils.isEmpty(map), resultCode, objects);
    }

    /**
     * not empty
     * @param map map
     * @param resultCode resultCode
     @param objects objects
     */
    public static void notEmpty(Map map, GcTrainingResultCode resultCode, Object... objects) {
        isTrue(!CollectionUtils.isEmpty(map), resultCode, objects);
    }

    /**
     * isNumeric
     * @param str str
     * @param resultCode resultCode
     @param objects objects
     */
    public static void isNumeric(String str, GcTrainingResultCode resultCode, Object... objects) {
        isTrue(StringUtils.isNumeric(str), resultCode, objects);
    }

    /**
     * get log string
     *
     * @param objects objects
     * @return logString
     */
    private static String getLogString(Object... objects) {

        StringBuilder logStrBuilder = new StringBuilder();

        for (Object object : objects) {

            logStrBuilder.append(object);

        }

        return logStrBuilder.toString();
    }
}