
package xyz.mynt.gctraining2.service.api.template;


public interface NoArgFunction<R> {


    R apply();

}
