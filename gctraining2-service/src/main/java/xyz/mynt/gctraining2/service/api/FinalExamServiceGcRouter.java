package xyz.mynt.gctraining2.service.api;

import com.alipay.ap.router.client.annotations.Api;
import xyz.mynt.gctraining2.facade.common.BaseGcRouteResponse;
import xyz.mynt.gctraining2.facade.model.FinalExamPayRequest;
import xyz.mynt.gctraining2.facade.model.FinalExamPayResponse;

public interface FinalExamServiceGcRouter {

    @Api(name = "gctraining2.finalexam.pay", version = "1.0", path = "/gctraining2/finalexam/pay")
    BaseGcRouteResponse<FinalExamPayResponse> authenticate(FinalExamPayRequest request);
}
