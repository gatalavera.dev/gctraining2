package xyz.mynt.gctraining2.service;

import com.alibaba.common.logging.Logger;
import com.alibaba.common.logging.LoggerFactory;
import com.alipay.fc.common.util.log.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import xyz.mynt.gctraining2.facade.common.BaseGcRouteResponse;
import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.model.FinalExamPayRequest;
import xyz.mynt.gctraining2.facade.model.FinalExamPayResponse;
import xyz.mynt.gctraining2.facade.pay.FinalExamPayService;
import xyz.mynt.gctraining2.mq.MqService;
import xyz.mynt.gctraining2.mq.model.PublishRequest;
import xyz.mynt.gctraining2.service.api.template.ControllerLayerTemplate;

@RestController
public class RestCaller {

    /** LOGGER */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(RestCaller.class);

    @Autowired
    MqService mqService;

    @Autowired
    FinalExamPayService finalExamPayService;

    @RequestMapping(value = "/mqPublisher", method = RequestMethod.POST)
    public String serviceCaller(@RequestBody PublishRequest publishRequest)
    {
        LogUtil.info(LOGGER, "Topic:", publishRequest.getTopic(), "EventCode:", publishRequest.getEventCode(),
                    "message : ", publishRequest.getMessage(), "targetApp : ", publishRequest.getTargetApp());

        mqService.publish(publishRequest);
        return "Message Sent";
    }

    @RequestMapping(value = "/gctraining2/finalExam/pay")
    public BaseGcRouteResponse<FinalExamPayResponse> autheticate(@RequestBody  FinalExamPayRequest request) {

            LOGGER.info("FinalExamPayRequest: " + request.toString());
        GcTrainingResult<FinalExamPayResponse> result = ControllerLayerTemplate.execute(LOGGER,
                "FinalExamServiceGcRouter.authenticate",
                () -> finalExamPayService.validateFinalPay(request));
        return new BaseGcRouteResponse<>(result, result.getData());
    }
}