package xyz.mynt.gctraining2.service.api;

import com.alipay.ap.router.client.annotations.Api;
import xyz.mynt.gctraining2.facade.common.BaseGcRouteResponse;
import xyz.mynt.gctraining2.facade.model.AuthenticateRequest;
import xyz.mynt.gctraining2.facade.model.AuthenticateResponse;
import xyz.mynt.gctraining2.mq.model.PublishDTO;
import xyz.mynt.gctraining2.mq.model.PublishRequest;


public interface AuthenticationServiceGcRouter {

    @Api(name = "gctraining2.auth.authenticate", version = "1.0", path = "/gctraining2/auth/authenticate")
    BaseGcRouteResponse<AuthenticateResponse> authenticate(AuthenticateRequest request);

    @Api(name = "gctraining2.publishMessageQueue", version = "1.0", path = "/gctraining2/publishMessageQueue")
    BaseGcRouteResponse<PublishDTO> publishMessageQueue(PublishRequest publishRequest);
}
