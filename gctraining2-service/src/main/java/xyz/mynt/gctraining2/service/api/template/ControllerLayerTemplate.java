
package xyz.mynt.gctraining2.service.api.template;

import com.alibaba.common.logging.Logger;
import com.alibaba.common.logging.LoggerFactory;
import com.alipay.fc.common.util.log.LogUtil;
import xyz.mynt.gctraining2.facade.common.GcTrainingException;
import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.common.GcTrainingResultCode;

public class ControllerLayerTemplate {

    private static final Logger DIGEST_LOGGER = LoggerFactory.getLogger("BIZ_DIGEST");

    public static <R> GcTrainingResult<R> execute(Logger logger, String scenario,
                                                  NoArgFunction<GcTrainingResult<R>> callback) {

        GcTrainingResult<R> result = null;

        try {

            result = callback.apply();

            if (result == null) {

                throw new GcTrainingException(GcTrainingResultCode.SYSTEM_ERROR,
                    scenario + " result is null");

            }

            return result;

        } catch (GcTrainingException e) {

            LogUtil.warn(logger, e, scenario, " error");
            result = new GcTrainingResult<>(e);

        } catch (Throwable e) {

            LogUtil.error(logger, e, scenario, " error");
            result = new GcTrainingResult<>(new GcTrainingException(GcTrainingResultCode.SYSTEM_ERROR, e));

        }

        LogUtil.info(DIGEST_LOGGER, String.join(",", scenario, String.valueOf(result.getSuccess()),
            result.getResultStatus(), result.getResultCode(), result.getResultMsg()));

        return result;
    }

}
