package xyz.mynt.gctraining2.service.authentication;

import com.alipay.ap.router.client.utils.LogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import xyz.mynt.gctraining2.dal.dataobject.UsernameAndPassDO;
import xyz.mynt.gctraining2.dal.mybatis.UsernameAndPassMybatisDAO;
import xyz.mynt.gctraining2.facade.authentication.AuthenticationService;
import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.common.GcTrainingResultCode;
import xyz.mynt.gctraining2.facade.model.AuthenticateRequest;
import xyz.mynt.gctraining2.facade.model.AuthenticateResponse;

import java.util.UUID;

public class AuthenticationServiceImpl implements AuthenticationService {
    private static final Logger logger = LoggerFactory.getLogger("GCTRAINING");

    @Autowired
    UsernameAndPassMybatisDAO usernameAndPassMybatisDAO;

    @Override
    public GcTrainingResult<AuthenticateResponse> authenticate(AuthenticateRequest authenticateRequest) {

        AuthenticateResponse authenticateResponse = new AuthenticateResponse();
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        authenticateResponse.setTransId(randomUUIDString);

        UsernameAndPassDO usernameAndPassDO = usernameAndPassMybatisDAO.selectUsernameAndPassword(authenticateRequest.getLoginName());

        if (usernameAndPassDO == null) {
            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_USERNAME, authenticateResponse);
        } else if (usernameAndPassDO.getUsername().equals(authenticateRequest.getLoginName()) &&
                usernameAndPassDO.getPassword().equals(authenticateRequest.getLoginPassword())) {
            return new GcTrainingResult<>(GcTrainingResultCode.SUCCESS, authenticateResponse);
        }

        LogUtil.info(logger, authenticateRequest.toString());
        return new GcTrainingResult<>(GcTrainingResultCode.FAILED, authenticateResponse);
    }

    //    @Override
//    public GcTrainingResult<AuthenticateResponse> authenticate(AuthenticateRequest authenticateRequest) {
//
//        AuthenticateResponse authenticateResponse = new AuthenticateResponse();
//        UUID uuid = UUID.randomUUID();
//        String randomUUIDString = uuid.toString();
//        authenticateResponse.setTransId(randomUUIDString);
//
//        LogUtil.info(logger, authenticateRequest.toString());
//
//        if (!(GcTraining2userNameAndPassResource.validateUsername(authenticateRequest.getLoginName()))) {
//            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_USERNAME, authenticateResponse);
//        } else if (!(GcTraining2userNameAndPassResource.validatePassword(authenticateRequest.getLoginName(), authenticateRequest.getLoginPassword()))) {
//            return new GcTrainingResult<>(GcTrainingResultCode.INVALID_PASSWORD, authenticateResponse);
//        }
//
//        LogUtil.info(logger, authenticateResponse.toString());
//
//        return new GcTrainingResult<>(GcTrainingResultCode.SUCCESS, authenticateResponse);
//    }
}
