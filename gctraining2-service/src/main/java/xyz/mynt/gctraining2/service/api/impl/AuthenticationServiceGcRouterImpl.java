package xyz.mynt.gctraining2.service.api.impl;

import com.alibaba.common.logging.Logger;
import com.alibaba.common.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.mynt.gctraining2.facade.authentication.AuthenticationService;
import xyz.mynt.gctraining2.facade.common.BaseGcRouteResponse;
import xyz.mynt.gctraining2.facade.common.GcTrainingResult;
import xyz.mynt.gctraining2.facade.common.GcTrainingResultCode;
import xyz.mynt.gctraining2.facade.model.AuthenticateRequest;
import xyz.mynt.gctraining2.facade.model.AuthenticateResponse;
import xyz.mynt.gctraining2.mq.MqService;
import xyz.mynt.gctraining2.mq.model.PublishDTO;
import xyz.mynt.gctraining2.mq.model.PublishRequest;
import xyz.mynt.gctraining2.service.api.AuthenticationServiceGcRouter;
import xyz.mynt.gctraining2.service.api.template.ControllerLayerTemplate;

@Service
public class AuthenticationServiceGcRouterImpl implements AuthenticationServiceGcRouter {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AuthenticationServiceGcRouterImpl.class);

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    MqService mqService;


    @Override
    public BaseGcRouteResponse<AuthenticateResponse> authenticate(AuthenticateRequest request) {
        GcTrainingResult<AuthenticateResponse> result = ControllerLayerTemplate.execute(LOGGER,
                "AuthenticationServiceGcRouter.authenticate",
                () -> authenticationService.authenticate(request));
        return new BaseGcRouteResponse<>(result, result.getData());
    }

    @Override
    public BaseGcRouteResponse<PublishDTO> publishMessageQueue(PublishRequest publishRequest) {
        GcTrainingResult<PublishDTO> result = ControllerLayerTemplate.execute(LOGGER,
                "AuthenticationServiceGcRouter.publishMessageQueue",
                () -> publishMessage(publishRequest));

        return new BaseGcRouteResponse<>(result, result.getData());
    }

    private GcTrainingResult<PublishDTO> publishMessage(PublishRequest publishRequest) {
        mqService.publish(publishRequest);
        return new GcTrainingResult<>(GcTrainingResultCode.SUCCESS, new PublishDTO(publishRequest));
    }
}