package xyz.mynt.gctraining2.dal.mybatis;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import xyz.mynt.gctraining2.dal.daointerface.TransactionDAO;
import xyz.mynt.gctraining2.dal.dataobject.TransactionDO;
import xyz.mynt.gctraining2.mq.AsyncPayService;

@Repository
public class TransactionMybatisDAO implements TransactionDAO {

    private static final Logger logger = LoggerFactory.getLogger(AsyncPayService.class);

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    public TransactionDO selectTransaction(String trans_id) {
        TransactionDAO mapper = sqlSessionTemplate.getMapper(TransactionDAO.class);
        return mapper.selectTransaction(trans_id);
    }

    @Override
    public void insertTransaction(TransactionDO transactionDO) {
        sqlSessionTemplate.insert("xyz.mynt.gctraining2.dal.daointerface.TransactionDAO.insertTransaction", transactionDO);
    }
}
