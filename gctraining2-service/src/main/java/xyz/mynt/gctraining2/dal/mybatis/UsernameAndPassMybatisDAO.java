package xyz.mynt.gctraining2.dal.mybatis;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import xyz.mynt.gctraining2.dal.daointerface.UsernameAndPassDAO;
import xyz.mynt.gctraining2.dal.dataobject.UsernameAndPassDO;

public class UsernameAndPassMybatisDAO implements UsernameAndPassDAO {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    public UsernameAndPassDO selectUsernameAndPassword(String username) {
       UsernameAndPassDAO mapper = sqlSessionTemplate.getMapper(UsernameAndPassDAO.class);
        return mapper.selectUsernameAndPassword(username);
    }
}