package xyz.mynt.gctraining2.dal.daointerface;

import org.apache.ibatis.annotations.*;
import xyz.mynt.gctraining2.dal.dataobject.TransactionDO;

public interface TransactionDAO {

    TransactionDO selectTransaction(@Param("trans_id") String trans_id);

    void insertTransaction(TransactionDO transactionDO);
}
