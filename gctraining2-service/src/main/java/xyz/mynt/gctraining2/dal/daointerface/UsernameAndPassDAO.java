package xyz.mynt.gctraining2.dal.daointerface;

import org.apache.ibatis.annotations.Param;
import xyz.mynt.gctraining2.dal.dataobject.UsernameAndPassDO;

public interface UsernameAndPassDAO {

    UsernameAndPassDO selectUsernameAndPassword(@Param("username") String username);
}
