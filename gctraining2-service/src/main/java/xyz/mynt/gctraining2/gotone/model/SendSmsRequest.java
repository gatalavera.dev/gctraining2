package xyz.mynt.gctraining2.gotone.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class SendSmsRequest {

    /**
     * request id
     */
    private String requestId;

    /**
     * sender channel (different channel have different service mobile)
     */
    private SmsChannel sendingChannel;

    /**
     * receiver mobile
     */
    private String receiverMobile;

    /**
     * content
     */
    private String content;

    /**
     * Getter method for property <tt>requestId</tt>.
     *
     * @return property value of requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Setter method for property <tt>requestId</tt>.
     *
     * @param requestId value to be assigned to property requestId
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Getter method for property <tt>sendingChannel</tt>.
     *
     * @return property value of sendingChannel
     */
    public SmsChannel getSendingChannel() {
        return sendingChannel;
    }

    /**
     * Setter method for property <tt>sendingChannel</tt>.
     *
     * @param sendingChannel value to be assigned to property sendingChannel
     */
    public void setSendingChannel(SmsChannel sendingChannel) {
        this.sendingChannel = sendingChannel;
    }

    /**
     * Getter method for property <tt>receiverMobile</tt>.
     *
     * @return property value of receiverMobile
     */
    public String getReceiverMobile() {
        return receiverMobile;
    }

    /**
     * Setter method for property <tt>receiverMobile</tt>.
     *
     * @param receiverMobile value to be assigned to property receiverMobile
     */
    public void setReceiverMobile(String receiverMobile) {
        this.receiverMobile = receiverMobile;
    }

    /**
     * Getter method for property <tt>content</tt>.
     *
     * @return property value of content
     */
    public String getContent() {
        return content;
    }

    /**
     * Setter method for property <tt>content</tt>.
     *
     * @param content value to be assigned to property content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}