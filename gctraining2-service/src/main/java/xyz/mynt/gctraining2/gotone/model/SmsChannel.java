package xyz.mynt.gctraining2.gotone.model;

public enum SmsChannel {

    /**
     * gcash
     */
    GCASH("GCASH", "gcash"),

    /**
     * channel 2882
     */
    C2882("FUND", "channel 2882");

    /**
     * sms template prefix
     */
    private String templatePrefix;

    /**
     * channel description
     */
    private String desc;

    SmsChannel(String templatePrefix, String desc) {
        this.templatePrefix = templatePrefix;
        this.desc = desc;
    }

    /**
     * get by code
     *
     * @param code code
     * @return channel
     */
    public static SmsChannel getByCode(String code) {

        for (SmsChannel smsChannel : SmsChannel.values()) {

            if (smsChannel.getCode().equals(code)) {
                return smsChannel;
            }

        }

        return null;

    }

    /**
     * get code
     * @return name()
     */
    public String getCode() {
        return name();
    }

    /**
     * Getter method for property <tt>templatePrefix</tt>.
     *
     * @return property value of templatePrefix
     */
    public String getTemplatePrefix() {
        return templatePrefix;
    }

    /**
     * Getter method for property <tt>desc</tt>.
     *
     * @return property value of desc
     */
    public String getDesc() {
        return desc;
    }

}
