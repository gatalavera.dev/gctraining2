package xyz.mynt.gctraining2.mq;

import com.alipay.common.event.UniformEvent;
import com.alipay.common.event.UniformEventBuilder;
import com.alipay.common.event.UniformEventPublisher;
import com.alipay.common.event.impl.DefaultUniformEventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.mynt.gctraining2.facade.dto.AccountDTO;
import xyz.mynt.gctraining2.mq.model.PublishDTO;
import xyz.mynt.gctraining2.mq.model.PublishRequest;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class MqService {
    private static final Logger logger = LoggerFactory.getLogger(MqService.class);

    private UniformEventPublisher mqPublisher;

    private UniformEventBuilder uniformEventBuilder = new DefaultUniformEventBuilder();

    public boolean publish(PublishRequest publishRequest) {
        PublishDTO publishDTO = new PublishDTO(publishRequest);

        if (logger.isInfoEnabled()) {
            logger.info("Publish a message.");
        }
        /* Create a message instance. */
        final UniformEvent message = uniformEventBuilder.buildUniformEvent(publishRequest.getTopic(), publishRequest.getEventCode());

        /* Set the business object as an event payload. */
        message.setEventPayload(publishDTO.getPublishDetails());

        /* Mark that a runtime exception must be thrown when publishing failure. */
        message.setThrowExceptionOnFailed(true);

        boolean publishSuccess = false;
        try {

            /* Do publish action. */
            mqPublisher.publishUniformEvent(message);
            publishSuccess = true;
            logger.info("Public a message, success. TOPIC [{}] EVENTCODE [{}] id [{}] payload [{}]", new Object[]{
                    message.getTopic(), message.getEventCode(), message.getId(), message.getEventPayload()});
        } catch (Exception e) {
            logger.error("Publish a message, failure. TOPIC [{}] EVENTCODE [{}] error [{}]",
                    new Object[]{message.getTopic(), message.getEventCode(), e.getMessage()}, e);
        }
        return publishSuccess;
    }

    private AccountDTO buildDefaultAccount() {
        AccountDTO account = new AccountDTO();
        account.setId(UUID.randomUUID().toString());
        account.setAmount(new Random().nextDouble());
        account.setGmtCreate(new Date());
        return account;
    }

    public void setMqPublisher(UniformEventPublisher mqPublisher) {
        this.mqPublisher = mqPublisher;
    }
}
