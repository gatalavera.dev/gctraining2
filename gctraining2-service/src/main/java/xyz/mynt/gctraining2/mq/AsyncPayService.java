package xyz.mynt.gctraining2.mq;

import com.alipay.common.event.UniformEvent;
import com.alipay.common.event.UniformEventBuilder;
import com.alipay.common.event.UniformEventPublisher;
import com.alipay.common.event.impl.DefaultUniformEventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.mynt.gctraining16.facade.dto.AccountDTO;

public class AsyncPayService {

    private static final Logger logger = LoggerFactory.getLogger(AsyncPayService.class);

    private UniformEventPublisher mqPublisher;

    private UniformEventBuilder uniformEventBuilder = new DefaultUniformEventBuilder();

    public void publish(AccountDTO accountDTO) {
        final UniformEvent message = uniformEventBuilder.buildUniformEvent("TP_GCFE_PAY", "EC_GCFE_1234");
        message.setEventPayload(accountDTO);
        message.setThrowExceptionOnFailed(true);
        try {
            /* Do publish action. */
            mqPublisher.publishUniformEvent(message);
            logger.info("Publish a message, success. TOPIC [{}] EVENTCODE [{}] id [{}] payload [{}]", new Object[]{
                    message.getTopic(), message.getEventCode(), message.getId(), message.getEventPayload()});
        } catch (Exception e) {
            logger.error("Publish a message, failure. TOPIC [{}] EVENTCODE [{}] error [{}]",
                    new Object[]{message.getTopic(), message.getEventCode(), e.getMessage()}, e);
        }
    }

    public void setMqPublisher(UniformEventPublisher mqPublisher) {
        this.mqPublisher = mqPublisher;
    }
}
