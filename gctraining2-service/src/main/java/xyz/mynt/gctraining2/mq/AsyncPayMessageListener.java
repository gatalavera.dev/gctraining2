package xyz.mynt.gctraining2.mq;

import com.alibaba.common.logging.Logger;
import com.alibaba.common.logging.LoggerFactory;
import com.alipay.common.event.UniformEvent;
import com.alipay.common.event.UniformEventContext;
import com.alipay.common.event.UniformEventMessageListener;
import com.alipay.fc.common.lang.event.EventContext;
import com.alipay.fc.common.lang.event.EventContextUtil;
import com.alipay.fc.common.util.log.LogUtil;
import com.alipay.fc.gotone.common.model.SmsOutMessage;
import com.alipay.fc.gotone.common.result.GotoneServiceResult;
import com.alipay.fc.gotone.common.service.MessageSendService;
import org.springframework.beans.factory.annotation.Autowired;
import xyz.mynt.gctraining16.facade.common.GcTrainingException;
import xyz.mynt.gctraining16.facade.common.GcTrainingResultCode;
import xyz.mynt.gctraining16.facade.dto.TransactionDTO;
import xyz.mynt.gctraining2.gotone.model.SendSmsRequest;
import xyz.mynt.gctraining2.gotone.model.SmsChannel;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AsyncPayMessageListener  implements UniformEventMessageListener {
    private static final Logger logger = LoggerFactory.getLogger(AsyncPayMessageListener.class);
    /**
     * tnt inst id
     */
    public static final String TNT_INST_ID  = "MYNTW3PH";

    @Autowired
    MessageSendService messageSendService;

    @Override
    public void onUniformEvent(UniformEvent message, UniformEventContext context) throws Exception {
        /* get TOPIC, EVENTCODE and payload from the message instance */
        final String topic = message.getTopic();
        final String eventcode = message.getEventCode();
        final String id = message.getId();

        //get DTO as payload
        final Object businessObject = message.getEventPayload();
        TransactionDTO transactionDTO = (TransactionDTO) businessObject;

        // call sendMessage to send SMS
        GotoneServiceResult gotoneResult = messageSendService.sendMessage(convertToSmsOutMessage(composeSMSRequest(transactionDTO)));

        logger.info("Receive a message, TOPIC [{}] EVENTCODE [{}] id [{}] payload [{}]", new Object[]{topic,
                eventcode, id, businessObject});

        logger.info(gotoneResult.isSuccess() ? "Glads, Successfully sent SMS..." : "Failed to send an SMS...");
        logger.info("Result Code: " + gotoneResult.getReadableResultCode());
        logger.info("Error Code: " + gotoneResult.getErrorContext().fetchCurrentErrorCode());
    }

    /**
     * sms request convert to gotone request
     *
     * @param smsRequest smsRequest
     * @return sms out message
     */
    private SmsOutMessage convertToSmsOutMessage(SendSmsRequest smsRequest) {

        try {
            EventContext context = EventContextUtil.getEventContextFromTracer();
            context.setTntInstId(TNT_INST_ID);
            EventContextUtil.saveEventContextToTracer(context);
        } catch (Throwable e) {
            LogUtil.error(logger, e, "gotone.SmsService set instId to request sofa context error");
            throw new GcTrainingException(GcTrainingResultCode.SYSTEM_ERROR,
                    "set inst id to request context error", e);
        }

        SmsOutMessage smsOutMessage = new SmsOutMessage();
        smsOutMessage.setServiceCode(smsRequest.getSendingChannel().getTemplatePrefix()
                + "_NO_TEMPLATE");
        smsOutMessage.setRequestId(smsRequest.getRequestId());

        if (!smsRequest.getReceiverMobile().contains("-")) {
            smsOutMessage.setCountryCode("63");
            smsOutMessage.setMobile(smsRequest.getReceiverMobile());
        } else {
            String[] mobileNoSplit = smsRequest.getReceiverMobile().split("-");
            smsOutMessage.setCountryCode(mobileNoSplit[0]);
            smsOutMessage.setMobile(mobileNoSplit[1]);
        }

        Map<String, Object> templateArgs = new HashMap<>();
        templateArgs.put("content", smsRequest.getContent());
        smsOutMessage.setTemplateArgs(templateArgs);
        return smsOutMessage;
    }

    /**
     * compose SMS Request
     *
     * @param transactionDTO transactionDTO
     * @return sendSmsRequest
     */
    private SendSmsRequest composeSMSRequest(TransactionDTO transactionDTO) {
        SendSmsRequest sendSmsRequest = new SendSmsRequest();

        // Send SMS to gotone
        String message = " Sending SMS FROM GLADS";
        sendSmsRequest.setReceiverMobile(transactionDTO.getMobileNo());
        sendSmsRequest.setContent(message);
        sendSmsRequest.setSendingChannel(SmsChannel.GCASH);
        sendSmsRequest.setRequestId(UUID.randomUUID().toString());
        return sendSmsRequest;
    }
}
