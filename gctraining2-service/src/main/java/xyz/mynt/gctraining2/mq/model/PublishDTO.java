package xyz.mynt.gctraining2.mq.model;

import java.util.HashMap;

public class PublishDTO {
    private HashMap<String, String> publishDetails;
    //private PublishRequest publishRequest;


    public PublishDTO(PublishRequest publishRequest) {
        this.publishDetails = new HashMap<>();
        publishDetails.put("topic", publishRequest.getTopic());
        publishDetails.put("eventCode", publishRequest.getEventCode());
        publishDetails.put("targetApp", publishRequest.getTargetApp());
        publishDetails.put("message", publishRequest.getMessage());
    }


    public HashMap<String, String> getPublishDetails() {
        return publishDetails;
    }

    public void setPublishDetails(HashMap<String, String> publishDetails) {
        this.publishDetails = publishDetails;
    }
}